#lang scribble/manual

@(require
  racket/sandbox
  scribble/eval
  (for-label tjson)
  (for-label (only-meta-in 0 typed/racket)))

@(define example-eval
(parameterize ([sandbox-output       'string]
               [sandbox-error-output 'string]
               [sandbox-memory-limit #f])
(make-evaluator 'typed/racket #:requires (list "main.rkt"))))

@title{tjson}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{Introduction}

A typed Json parser and emitter.

@subsection{Object Mappings}

A Json Object is reified as a Racket hashmap, Json array to a Racket list and Strings, Booleans and Numbers mapped to Racket's types.  A special symbol is reserved to indicating a Json null value.

A parsed Json graph follows the naive recursive data structure definition as Json is a string, number, boolean (true or false), list of Json or an object with a multiplicity of unique symbol labeled Json.

@defmodule[tjson]

@section{Json Parsed Data Structure}

@defthing[Json (Rec (U String Boolean JsNull Number (Listof Json)
                       (HashTable Symbol Json)))]{
A translation of a parsed Json data structure reflectiing it recursive nature.
}

@defthing[JsObject (HashTable Symbol Json)]{
A define-type alias reflecting a Json Object as a hashmap of symbols and Json values.
}		       

@defthing[JsList (Listof Json)]{
A define-type alias reflecting a Json Array as a Racket List of Json.
}

@defthing[JsNull 'JsNull]{
A Json null value is reflected in Racket as a special reserved symbol.
JsNull is a define-type alias for the symbol value at the type level.  In Typed Racket each symbol is given a unique type containing only that symbol.

While the type Symbol has all symbols as inhabiting values, the type 'JsNull has only the single inhabiting value 'JsNull.
}

@section{Json Parsing}

@defproc[(string->json [json String]) Json]{
Parse a string representation of Json into a Typed Racket Json structure.
}

@examples[#:eval example-eval
(string->json "42")
(string->json "\"Mary had a little lamb\"")
(string->json "[\"lamb\", \"cat\", \"dog\"]")
(string->json "{\"cat\" : \"meow\", \"dog\" : \"bark\", \"lamb\" : \"bleet\"}")
]

@section{Json Construction}

@defproc[(jsobject [attributes (Listof (Pair Symbol Json))]) JsObject]{
Creates a JsObject instance from a list of symbol and json value associations.  Note as a JsObject is just a transparent type alias for a HashMap any of the standard Racket hashmap construction procedures maybe used as well such as (@racket[hash] ...) or (make-hash ...).
}

@examples[#:eval @example-eval
(jsobject '((cat . "meow") (dog . "bark") (lamb . "bleet") (turtle . JsNull)))
]

@section{JsObject}

@subsection{JsObject Attribute Lookup}

Lookup an JsObject attribute's (key) json value.

@defproc[(jsattribute? [jobj JsObject] [key Symbol]) Boolean]{
Whether the attribute exists for the jsobject.
}

@defproc[(jsattribute [jobj JsObject] [key Symbol]) (Option Json)]{
Returns the value of the given attribute key if it exists otherwise #f.

Note: For boolean values attributes, given how Option values are implementinted in Typed Racket, the #f return value cannot be used to descriminate between an existing attribute whose value is #f or a missing attribute.
}

@defproc[(jsattribute-orelse [jobj JsObject] [key Symbol] [default (-> Json)]) Json]{
Returns the json value of the attribute key if it exists otherwise the json returned by the provided thunk.
}

@subsection{JsObject Modification}

By design JsObjects are currently implemented as mutable hashmaps and may be mutated.  Recall that a JsObject is a type alias for a Racket mutable hashmap.  For details concerning concurrent modification see the Racket documantation for hashmaps.

The Racket implemetation of hashmap operations is rich and only a subset are currently wrapped in this library.  Additional capabilities such as union, jsobject-union! will added at a later time.

@defproc[(jsobject-add! [jobj JsObject] [key Symbol] [json Json]) Void]{
Adds the attribute to a JsObject overwriting an existing attribute.
}

@defproc[(jsobject-remove! [jobj JsObject] [key Symbol]) Void]{
Removes the attribute from the jsobject if it exists.  It is not an error
to attempt to remove a non-existing attribute.
}

@defproc[(jsobject-update! [jobj JsObject] [key Symbol] [updater (-> Json Json)]) Void]{
Modifies an existing attribute value.  A fail:contract exception is thrown if the attribute does not exist.
}

@examples[#:eval @example-eval
(define j (jsobject '((cat . "meow"))))
(jsobject-update! j 'cat (λ (says) (string-append (cast says String) " purr")))
j
]

@section{Json Emission}

@defproc[(json->string [json Json]) String]{
Emit a string representation of a Json data structure.
}

@examples[#:eval @example-eval
(json->string (jsobject '((cat . "meow") (dog . "bark") (lamb . "bleet") (turtle . JsNull))))
]

@section{Port Serialization}

@defproc[(write-json [json Json] [outp Output-Port]) Void]{
Serialize a Json data structure out the provided port as a json string.
}

@defproc[(read-json [inp Input-Port]) Json]{
Read a json value from the input port.
}
